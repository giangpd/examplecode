DECLARE @Id INT = 14;
WITH Clinic_CTE
AS (SELECT a.ClinicId,
           a.ParentId,
           a.Name
    FROM cat_Clinic a
    WHERE ClinicId = @Id
    UNION ALL
    SELECT a.ClinicId,
           a.ParentId,
           a.Name
    FROM cat_Clinic a
        JOIN Clinic_CTE c
            ON a.ParentId = c.ClinicId)
SELECT *
FROM Clinic_CTE;

select * from cat_Clinic a inner join cte b on a.ClinicId = b.ClinicId